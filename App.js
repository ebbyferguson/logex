import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import StackScreen from "./app/components/StackScreen";


export default function App() {
  return (
    <NavigationContainer>
      <StackScreen />
    </NavigationContainer>
  );
}
