---
comments: false
type: index
---

# AFI - React Native Expo

Follow the following steps after you clone this repository to run application successfully.
Note that steps will be provided for the application usage too.

# Setup/Installation

## Expo CLI installation
Before you clone the project, your environment should have NodeJS installed
if not, head to [NodeJS](https://nodejs.org/en/) for installation instructions

###### install expo-cli
 Run the following command in your terminal 
`npm install -g expo-cli`

###### Run Project
 Run the following command in your terminal 
`expo-start` or `npm-start`

# DONE


# Screenshots
![](screenshots/screenshot_1.png)

![](screenshots/screenshot_2.png)

![](screenshots/screenshot_3.png)

![](screenshots/screenshot_4.png)

![](screenshots/screenshot_5.png)

