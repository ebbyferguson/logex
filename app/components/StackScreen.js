import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import SplashScreen from "../views/SplashScreen";
import SignUpScreen from "../views/SignUpScreen";
import SignInScreen from "../views/SignInScreen";
import HomeScreen from "../views/HomeScreen";
import DetailsScreen from "../views/DetailsScreen";

const Stack = createStackNavigator();

function StackScreen() {
  return (
    <Stack.Navigator
    // headerMode="none"
    >
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ title: "SplashScreen", headerShown: false }}
      />
      <Stack.Screen
        name="SignUpScreen"
        component={SignUpScreen}
        options={{ title: "SplashScreen", headerShown: false }}
      />
      <Stack.Screen
        name="SignInScreen"
        component={SignInScreen}
        options={{ title: "SignInScreen", headerShown: false }}
      />
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{ title: "HomeScreen", headerShown: false }}
      />
      <Stack.Screen
        name="DetailsScreen"
        component={DetailsScreen}
        options={{
          title: "Edit Sheet",
          headerTintColor:"#05375A",
          headerShown: true,
          headerBackTitle: "Back",
        }}
      />
    </Stack.Navigator>
  );
}

export default StackScreen;
