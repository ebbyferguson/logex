import moment from "moment";
import { StyleSheet, Text, TouchableOpacity, TextInput } from "react-native";
import * as SQLite from "expo-sqlite";
import React, { Component } from "react";
import { View } from "react-native-animatable";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import DateTimePicker from "react-native-modal-datetime-picker";
import Loader from "../components/Loader";

const db = SQLite.openDatabase("database.db");
console.log("----------- 1- Create/Open DataBase---------------");
moment.locale("en");

class DetailsScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    var startTee = this.props.route.params.start_time;
    var endTee = this.props.route.params.end_time;

    var showStartTee = moment(startTee).format("hh:mm a");
    var showEndTee = moment(endTee).format("hh:mm a");

    //converting rate to string
    var rateNum = this.props.route.params.rate;
    var rateString = rateNum + "";

    //converting id to string
    var idNum = this.props.route.params.id;
    var idString = idNum + "";

    this.setState({
      employee_id: this.props.route.params.employee_id,
      company: this.props.route.params.company,
      rate: rateString,
      date: this.props.route.params.date,
      start_time: this.props.route.params.start_time,
      end_time: this.props.route.params.end_time,
      created_date: this.props.route.params.created_date,
      showStart: showStartTee,
      showEnd: showEndTee,
      id: idString,
    });
  }

  state = {
    employee_id: "",
    company: "",
    rate: "",
    date: "",
    start_time: "",
    end_time: "",
    created_date: "",
    updated_date: "",
    isLoading: false,
    showStart: "",
    showEnd: "",
    id: "",
    isDateTimePickerVisible: false,
    isStartTimePickerVisible: false,
    isEndTimePickerVisible: false,
  };

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  showStartTimePicker = () => {
    this.setState({ isStartTimePickerVisible: true });
  };

  showEndTimePicker = () => {
    this.setState({
      isEndTimePickerVisible: true,
    });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  hideStartTimePicker = () => {
    this.setState({ isStartTimePickerVisible: false });
  };

  hideEndTimePicker = () => {
    this.setState({ isEndTimePickerVisible: false });
  };

  handleDatePicked = (value) => {
    console.log("A date has been picked: " + value);
    var selectedDate = moment(value).format("YYYY-MM-DD");
    this.setState({
      date: selectedDate,
    });
    this.hideDateTimePicker();
  };

  handleStartTimePicked = (time) => {
    console.log("Start time has been picked: ", time);
    var selectedTime = moment(time).format("LT");
    let ms = Date.parse(time);
    console.log("time in milliseconds: " + ms);
    this.setState({
      start_time: ms,
      showStart: selectedTime,
    });
    this.hideStartTimePicker();
  };

  handleEndTimePicked = (time) => {
    console.log("End time has been picked: ", time);
    var selectedTime = moment(time).format("LT");
    let ms = Date.parse(time);
    console.log("time in milliseconds: " + ms);
    this.setState({
      end_time: ms,
      showEnd: selectedTime,
    });
    this.hideEndTimePicker();
  };

  companyTextInputChange = (value) => {
    this.setState({
      company: value,
    });
  };

  rateTextInputChange = (value) => {
    this.setState({
      rate: value,
    });
  };

  ExecuteQuery = (sql, params = []) =>
    new Promise((resolve, reject) => {
      db.transaction((trans) => {
        trans.executeSql(
          sql,
          params,
          (trans, results) => {
            resolve(results);
            console.log(results.rows._array);
          },
          (error) => {
            reject(error);
            alert(error);
          }
        );
      });
    });

  /**
   * Example Of update query
   */
  async UpdateQuery() {
    let updateQuery = await this.ExecuteQuery(
      "update timelogs set rate = ?, company = ?, date = ?, start_time = ?, end_time = ?, created_date = ?, updated_date = ?, where id = ?",
      [
        this.state.rate,
        this.state.company,
        this.state.date,
        this.state.start_time,
        this.state.end_time,
        this.state.created_date,
        this.state.updated_date,
        this.state.id,
      ]
    );

    console.log(updateQuery);
  }

  updateEntry = () => {
    db.transaction(
      (tx) => {
        tx.executeSql(
          "update timelogs set rate = ?, company = ?, date = ?, start_time = ?, end_time = ?, created_date = ?, updated_date = ?, where id = ?",
          [
            this.state.rate,
            this.state.company,
            this.state.date,
            this.state.start_time,
            this.state.end_time,
            this.state.created_date,
            this.state.updated_date,
            this.state.id,
          ]
        );
        tx.executeSql("select * from timelogs", [], (_, { rows }) => {
          console.log(JSON.stringify(rows));
          console.log("Number of items: " + rows.length);
        });
      },
      null,
      showThis
    );
  };

  updateTimeSheet = () => {
    var valid = false;

    if (this.state.company == "") {
      alert("Please add company name");
    } else if (this.state.rate == 0) {
      alert("Please add rate per hour");
    } else if (this.state.start_time == "") {
      alert("Please set your start time");
    } else if (this.state.date == "") {
      alert("Please set date");
    } else if (this.state.end_time == "") {
      alert("Please set end time");
    } else {
      valid = true;
    }

    var updatedDate = new Date();

    this.setState({
      updated_date: updatedDate,
    });
    if (valid) {
      this.UpdateQuery();
    }
  };

  render() {
    return (
      <View
        style={{ flex: 1, backgroundColor: "#fff", flexDirection: "column" }}
      >
        <View>
          <View style={styles.panel}>
            <Text style={styles.text_footer}>Company Name</Text>
            <View style={styles.action}>
              <FontAwesome name="building" color="#05375A" size={20} />
              <TextInput
                placeholder={this.state.company}
                value={this.state.company}
                autoCapitalize="none"
                returnKeyType={"done"}
                style={styles.textInput}
                onChangeText={(value) => this.companyTextInputChange(value)}
              />
            </View>
            <Text style={styles.text_footer}>Rate Per Hour</Text>
            <View style={styles.action}>
              <Text style={styles.text_norm}>$ </Text>
              <TextInput
                placeholder={this.state.rate}
                autoCapitalize="none"
                value={this.state.rate}
                keyboardType={"number-pad"}
                returnKeyType={"done"}
                style={styles.textInput}
                onChangeText={(value) => this.rateTextInputChange(value)}
              />
            </View>
            <View style={styles.action}>
              <TouchableOpacity onPress={this.showDateTimePicker}>
                <Text style={styles.text_footer}>EDIT DATE</Text>
              </TouchableOpacity>
              <Text
                style={{
                  paddingLeft: 15,
                  paddingTop: 15,
                  fontSize: 18,
                  justifyContent: "center",
                }}
              >
                {this.state.date}
              </Text>
            </View>
            <View style={styles.action}>
              <TouchableOpacity onPress={this.showStartTimePicker}>
                <Text style={styles.text_footer}>EDIT START TIME</Text>
              </TouchableOpacity>
              <Text
                style={{
                  paddingLeft: 15,
                  paddingTop: 15,
                  fontSize: 18,
                  justifyContent: "center",
                }}
              >
                {this.state.showStart}
              </Text>
            </View>
            <View style={styles.action}>
              <TouchableOpacity onPress={this.showEndTimePicker}>
                <Text style={styles.text_footer}>EDIT END TIME</Text>
              </TouchableOpacity>
              <Text
                style={{
                  paddingLeft: 15,
                  paddingTop: 15,
                  fontSize: 18,
                  justifyContent: "center",
                }}
              >
                {this.state.showEnd}
              </Text>
            </View>
            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.handleDatePicked}
              onCancel={this.hideDateTimePicker}
              mode="date"
            />
            <DateTimePicker
              isVisible={this.state.isStartTimePickerVisible}
              onConfirm={this.handleStartTimePicked}
              onCancel={this.hideStartTimePicker}
              mode="time"
            />
            <DateTimePicker
              isVisible={this.state.isEndTimePickerVisible}
              onConfirm={this.handleEndTimePicked}
              onCancel={this.hideEndTimePicker}
              mode="time"
              minimumDate={new Date(this.state.start_time)}
            />
          </View>
        </View>
        <View style={{ flex: 1, justifyContent: "flex-end", marginBottom: 50 }}>
          <TouchableOpacity
            style={styles.buttonUpdate}
            onPress={() => {
              this.UpdateQuery;
            }}
          >
            <Text style={styles.updateButtonTitle}>Update</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonCancel}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Text style={styles.cancelButtonTitle}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function showThis() {
  console.log("Forced update");
}
export default DetailsScreen;

const styles = StyleSheet.create({
  buttonUpdate: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "dodgerblue",
    alignItems: "center",
    marginVertical: 10,
    marginHorizontal: 30,
  },
  buttonCancel: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "white",
    alignItems: "center",
    marginVertical: 10,
    marginHorizontal: 30,
  },
  updateButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  cancelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "red",
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 14,
    marginTop: 25,
  },
  text_norm: {
    color: "#05375a",
    fontSize: 24,
    fontWeight: "bold",
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
    fontSize: 24,
  },
});
