import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  Dimensions,
  TextInput,
  FlatList,
  StatusBar,
} from "react-native";
import * as Animatable from "react-native-animatable";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import BottomSheet from "reanimated-bottom-sheet";
import Animated from "react-native-reanimated";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import DateTimePicker from "react-native-modal-datetime-picker";
import * as SQLite from "expo-sqlite";
import Loader from "../components/Loader";
import moment from "moment";
import AsyncStorage from "@react-native-community/async-storage";
import { Card } from "react-native-paper";

const db = SQLite.openDatabase("database.db");
console.log("----------- 1- Create/Open DataBase---------------");
moment.locale("en");

class HomeScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getEmployeeId();
  }

  getEmployeeId = async () => {
    var value = "";
    try {
      value = await AsyncStorage.getItem("employeeID");
      console.log("got employee here: " + value);
      this.setState({
        employee_id: value,
      });
      console.log("got employee here in state: " + this.state.employee_id);
      this.isDBEmpty();
    } catch (e) {
      console.log(e);
      this.props.navigation.navigate("SplashScreen");
    }
  };
  state = {
    employee_id: "",
    company: "",
    rate: 0,
    date: "",
    start_time: "",
    end_time: "",
    created_date: "",
    updated_date: "",
    isLoading: false,
    dbEmpty: false,
    data: "",
    showStart: "",
    showEnd: "",
    isDateTimePickerVisible: false,
    isStartTimePickerVisible: false,
    isEndTimePickerVisible: false,
  };

  bs = React.createRef();
  fall = new Animated.Value(1);

  showSheet = () => {
    bs = React.createRef();
    fall = new Animated.Value(1);
  };

  renderInner = () => (
    <View style={styles.panel}>
      <Text style={styles.panelTitle}>Fill in the details below</Text>
      <Text style={styles.panelSubtitle}>
        and tap "Done" to add a new timesheet
      </Text>
      <Text style={styles.text_footer}>Company Name</Text>
      <View style={styles.action}>
        <FontAwesome name="building" color="#05375A" size={20} />
        <TextInput
          placeholder="Enter company name"
          autoCapitalize="none"
          returnKeyType={"done"}
          style={styles.textInput}
          onChangeText={(value) => this.companyTextInputChange(value)}
        />
      </View>
      <Text style={styles.text_footer}>Rate Per Hour</Text>
      <View style={styles.action}>
        <Text style={styles.text_norm}>$ </Text>
        <TextInput
          placeholder="Enter your rate here"
          autoCapitalize="none"
          value={this.state.ratePerhour}
          keyboardType={"number-pad"}
          returnKeyType={"done"}
          style={styles.textInput}
          onChangeText={(value) => this.rateTextInputChange(value)}
        />
      </View>
      <View style={styles.action}>
        <TouchableOpacity onPress={this.showDateTimePicker}>
          <Text style={styles.text_footer}>ADD DATE</Text>
        </TouchableOpacity>
        <Text
          style={{
            paddingLeft: 15,
            paddingTop: 15,
            fontSize: 18,
            justifyContent: "center",
          }}
        >
          {this.state.date}
        </Text>
      </View>
      <View style={styles.action}>
        <TouchableOpacity onPress={this.showStartTimePicker}>
          <Text style={styles.text_footer}>SET START TIME</Text>
        </TouchableOpacity>
        <Text
          style={{
            paddingLeft: 15,
            paddingTop: 15,
            fontSize: 18,
            justifyContent: "center",
          }}
        >
          {this.state.showStart}
        </Text>
      </View>
      <View style={styles.action}>
        <TouchableOpacity onPress={this.showEndTimePicker}>
          <Text style={styles.text_footer}>SET END TIME</Text>
        </TouchableOpacity>
        <Text
          style={{
            paddingLeft: 15,
            paddingTop: 15,
            fontSize: 18,
            justifyContent: "center",
          }}
        >
          {this.state.showEnd}
        </Text>
      </View>
      <DateTimePicker
        isVisible={this.state.isDateTimePickerVisible}
        onConfirm={this.handleDatePicked}
        onCancel={this.hideDateTimePicker}
        mode="date"
      />
      <DateTimePicker
        isVisible={this.state.isStartTimePickerVisible}
        onConfirm={this.handleStartTimePicked}
        onCancel={this.hideStartTimePicker}
        mode="time"
      />
      <DateTimePicker
        isVisible={this.state.isEndTimePickerVisible}
        onConfirm={this.handleEndTimePicked}
        onCancel={this.hideEndTimePicker}
        mode="time"
        minimumDate={new Date(this.state.start_time)}
      />

      <TouchableOpacity
        style={styles.panelButton}
        onPress={() => {
          this.addNewEntry();
        }}
      >
        <Text style={styles.panelButtonTitle}>Done</Text>
      </TouchableOpacity>
    </View>
  );

  renderHeader = () => (
    <View style={styles.sheetHeader}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  showStartTimePicker = () => {
    this.setState({ isStartTimePickerVisible: true });
  };

  showEndTimePicker = () => {
    this.setState({
      isEndTimePickerVisible: true,
    });
  };

  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  hideStartTimePicker = () => {
    this.setState({ isStartTimePickerVisible: false });
  };

  hideEndTimePicker = () => {
    this.setState({ isEndTimePickerVisible: false });
  };

  handleDatePicked = (value) => {
    console.log("A date has been picked: " + value);
    var selectedDate = moment(value).format("YYYY-MM-DD");
    this.setState({
      date: selectedDate,
    });
    this.hideDateTimePicker();
  };

  handleStartTimePicked = (time) => {
    console.log("Start time has been picked: ", time);
    var selectedTime = moment(time).format("LT");
    let ms = Date.parse(time);
    console.log("time in milliseconds: " + ms);
    this.setState({
      start_time: ms,
      showStart: selectedTime,
    });
    this.hideStartTimePicker();
  };

  handleEndTimePicked = (time) => {
    console.log("End time has been picked: ", time);
    var selectedTime = moment(time).format("LT");
    let ms = Date.parse(time);
    console.log("time in milliseconds: " + ms);
    this.setState({
      end_time: ms,
      showEnd: selectedTime,
    });
    this.hideEndTimePicker();
  };

  companyTextInputChange = (value) => {
    this.setState({
      company: value,
    });
  };

  rateTextInputChange = (value) => {
    this.setState({
      rate: value,
    });
  };

  isDBEmpty = () => {
    db.transaction((tx) => {
      tx.executeSql("select * from timelogs", [], (_, { rows }) => {
        console.log(JSON.stringify(rows));
        console.log("Number of items: " + rows.length);
        var arr = rows._array;
        this.setState({
          data: arr,
        });
        console.log(this.state.data);
        if (rows.length < 1) {
          this.setState({
            dbEmpty: true,
          });
        } else {
          this.setState({
            dbEmpty: false,
          });
        }
        console.log(this.state.dbEmpty);
      });
    });
  };

  addNewTimeSheet = () => {
    var valid = false;

    if (this.state.company == "") {
      alert("Please add company name");
    } else if (this.state.rate == 0) {
      alert("Please add rate per hour");
    } else if (this.state.start_time == "") {
      alert("Please set your start time");
    } else if (this.state.date == "") {
      alert("Please set date");
    } else if (this.state.end_time == "") {
      alert("Please set end time");
    } else {
      valid = true;
    }

    var createdDate = new Date();

    this.setState({
      created_date: createdDate,
    });
    if (valid) {
      this.addNewEntry();
    }
  };

  diff_hours(dt2, dt1) {
    const startDate = moment(dt1);
    const timeEnd = moment(dt2);
    const diff = timeEnd.diff(startDate);
    const diffDuration = moment.duration(diff);

    var hours = diffDuration.hours();
    return hours;
  }

  addNewEntry = () => {
    db.transaction(
      (tx) => {
        tx.executeSql(
          "insert into timelogs (employee_id, rate, company, date, start_time, end_time, created_date, updated_date) values (?, ?, ?, ?, ?, ?, ?, ?)",
          [
            this.state.employee_id,
            this.state.rate,
            this.state.company,
            this.state.date,
            this.state.start_time,
            this.state.end_time,
            this.state.created_date,
            this.state.updated_date,
          ]
        );
        // select company, sum(rate * DATEDIFF(HOUR, start_time,end_time) from timelogs group by company;
        tx.executeSql("select * from timelogs", [], (_, { rows }) => {
          console.log(JSON.stringify(rows));
          console.log("Number of items: " + rows.length);
          var arr = rows._array;
          this.setState({
            data: arr,
          });
          console.log(this.state.data);
        });
      },
      null,
      showThis
    );
    this.bs.current.snapTo(1);
    this.isDBEmpty;
  };

  clearData = () => {
    this.setState({
      company: "",
      rate: 0,
      date: "",
      start_time: "",
      end_time: "",
      created_date: "",
      updated_date: "",
    });
  };

  getTimeSheets = () => {
    db.transaction((tx) => {
      tx.executeSql("select * from timelogs", [], (_, { rows }) => {
        var arr = rows._array;
        this.setState({
          data: arr,
        });
        console.log(this.state.data);
      });
    });
  };

  interfaceNoData = () => (
    <View style={styles.container}>
      <Loader loading={this.state.isLoading} />
      <BottomSheet
        ref={this.bs}
        snapPoints={[680, 0]}
        initialSnap={1}
        callbackNode={this.fall}
        enabledGestureInteraction={true}
        renderContent={this.renderInner}
        renderHeader={this.renderHeader}
      />
      <View style={styles.header}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Animated.View
          style={{
            margin: 20,
            opacity: Animated.add(0.1, Animated.multiply(this.fall, 1.0)),
          }}
        >
          <Animatable.Text animation="pulse" style={styles.titleNoData}>
            Oopss!!!..
          </Animatable.Text>
          <Animatable.Image
            animation="zoomIn"
            source={require("../assets/images/no_data.png")}
            style={styles.logo}
          />
          <Text style={styles.textNoData}>You have no timesheets </Text>
          <Text style={styles.textNoData}>
            Tap "Add New" button to add a new timesheet{" "}
          </Text>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => {
                this.clearData();
                this.bs.current.snapTo(0);
              }}
            >
              <View style={styles.action}>
                <MaterialIcons name="add" color="dodgerblue" size={24} />
                <Text style={styles.textButton}>Add New</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </View>
    </View>
  );

  showTimeSheet = () => (
    <View style={styles.timeSheetContainer}>
      <BottomSheet
        ref={this.bs}
        snapPoints={[530, 0]}
        initialSnap={1}
        callbackNode={this.fall}
        enabledGestureInteraction={true}
        renderContent={this.renderInner}
        renderHeader={this.renderHeader}
      />
      <StatusBar backgroundColor="#F5F5F5" barStyle="dark-content" />
      <View
        style={{
          flexDirection: "row",
          paddingLeft: 5,
          paddingTop: 20,
        }}
      >
        <Text style={styles.title}>Timesheets</Text>
        <View
          style={{
            flex: 1,
            paddingRight: 15,
            alignItems: "flex-end",
            marginBottom: 5,
            justifyContent: "center",
            elevation: 4,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              this.bs.current.snapTo(0);
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <MaterialIcons name="add" color="dodgerblue" size={24} />
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 20,
                  color: "dodgerblue",
                  textAlign: "center",
                  justifyContent: "center",
                }}
              >
                Add New
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
        keyExtractor={(item) => item.id}
        data={this.state.data}
        renderItem={({ item }) => (
          <Card
            style={styles.cardStyle}
            onPress={() => {
              this.props.navigation.navigate("DetailsScreen", item);
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: "100%",
                height: "100%",
                backgroundColor: "#fff",
                borderRadius: 10,
                marginLeft: 3,
                marginBottom: 5,
                marginTop: 5,
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  paddingLeft: 20,
                  paddingTop: 10,
                }}
              >
                <Text style={{ color: "#BDBDBD" }}>company name</Text>
                <Text
                  style={{
                    color: "#448AFF",
                    fontWeight: "bold",
                    fontSize: 26,
                  }}
                >
                  {item.company}
                </Text>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    paddingLeft: 10,
                    paddingTop: 10,
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      color: "#05375a",
                      fontWeight: "bold",
                      fontSize: 45,
                    }}
                  >
                    {this.diff_hours(item.end_time, item.start_time)}
                  </Text>
                  <Text style={{ flex: 2 }}> hr(s)</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    paddingLeft: 10,
                    paddingTop: 10,
                    paddingBottom: 10,
                    alignItems: "flex-end",
                  }}
                >
                  <Text style={{ color: "#BDBDBD" }}>Date: </Text>
                  <Text style={{ color: "#BDBDBD" }}>{item.date}</Text>
                </View>
              </View>
              <View
                style={{
                  width: 4,
                  height: "91%",
                  backgroundColor: "#e0e0e0",
                  borderRadius: 10,
                  marginBottom: 15,
                  marginTop: 5,
                  alignSelf: "center",
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  paddingLeft: 20,
                  paddingTop: 10,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Text style={{ color: "#BDBDBD" }}></Text>
                <Text
                  style={{
                    color: "#448AFF",
                    fontWeight: "bold",
                    fontSize: 26,
                  }}
                ></Text>
                <Text style={{ color: "#BDBDBD" }}>total cost</Text>
                <View
                  style={{
                    flexDirection: "row",
                    paddingTop: 10,
                    alignItems: "center",
                  }}
                >
                  <Text style={{ color: "#4CAF50", fontSize: 35 }}>$</Text>
                  <Text
                    style={{
                      color: "#4CAF50",
                      fontWeight: "bold",
                      fontSize: 35,
                    }}
                  >
                    {item.rate *
                      this.diff_hours(item.end_time, item.start_time)}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    paddingLeft: 10,
                    paddingTop: 10,
                    paddingBottom: 10,
                    alignItems: "flex-end",
                  }}
                >
                  <Text style={{ color: "#BDBDBD" }}>Rate: </Text>
                  <Text style={{ color: "#05375a" }}>$</Text>
                  <Text style={{ color: "#05375a" }}>{item.rate}</Text>
                  <Text style={{ color: "#05375a" }}> per hour</Text>
                </View>
              </View>
            </View>
          </Card>
        )}
      />
    </View>
  );

  render() {
    return !this.state.dbEmpty ? this.showTimeSheet() : this.interfaceNoData();
  }
}

function showThis() {
  console.log("Forced update");
}

export default HomeScreen;

const { height } = Dimensions.get("screen");
const height_img = height * 0.4;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#F5F5F5",
    alignItems: "center",
    justifyContent: "center",
  },
  timeSheetContainer: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    marginTop: 35,
    width: "100%",
    height: "100%",
    justifyContent: "flex-start",
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  logo: {
    width: height_img,
    height: height_img,
    backgroundColor: "#F5F5F5",
  },
  textNoData: {
    color: "grey",
    fontWeight: "normal",
    top: 14,
    fontSize: 13,
    textAlign: "center",
    justifyContent: "center",
  },
  title: {
    flex: 1,
    color: "#05375a",
    fontWeight: "bold",
    marginBottom: 5,
    fontSize: 24,
    marginLeft: 15,
    textAlign: "left",
    justifyContent: "center",
  },
  titleNoData: {
    color: "#05375a",
    fontWeight: "bold",
    marginBottom: 20,
    fontSize: 28,
    textAlign: "center",
    justifyContent: "center",
  },
  button: {
    alignItems: "center",
    marginTop: 50,
    backgroundColor: "#F5F5F5",
  },
  panelHeader: {
    alignItems: "center",
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: "#00000040",
    marginBottom: 10,
    marginTop: 5,
  },
  panel: {
    padding: 20,
    backgroundColor: "#FFFFFF",
    paddingTop: 20,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    color: "#05375a",
  },
  panelSubtitle: {
    fontSize: 14,
    color: "gray",
    height: 30,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: "dodgerblue",
    alignItems: "center",
    marginVertical: 30,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  textButton: {
    fontSize: 20,
    fontWeight: "bold",
    color: "dodgerblue",
  },
  sheetHeader: {
    backgroundColor: "#FFFFFF",
    shadowColor: "#333333",
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 14,
    marginTop: 15,
  },
  text_norm: {
    color: "#05375a",
    fontSize: 18,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
    fontSize: 20,
  },
  viewSideLine: {
    width: 5,
    color: "grey",
    height: "100%",
    borderRadius: 15,
  },
  cardStyle: {
    flex: 1,
    margin: 10,
    flexDirection: "row",
    borderRadius: 15,
    shadowRadius: 5,
    elevation: 4,
  },
});
