import React, { Component, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  StatusBar,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import Loader from "../components/Loader";
import AsyncStorage from "@react-native-community/async-storage";

class SignInScreen extends Component {
  state = {
    employeeID: "",
    password: "",
    isLoading: false,
    checkTextInputChange: false,
    secureTextEntry: true,
  };

  constructor(props) {
    super(props);
  }

  updateSecureTextEntry = () => {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  };

  handleLogin = async () => {
    this.setState({
      isLoading: true,
    });
    if (this.state.employeeID == "" || this.state.password == "") {
      alert("Please fill all fields");
    } else {
      var employeeId = "";
      var password = "";
      try {
        employeeId = await AsyncStorage.getItem("employeeID");
        console.log("got employee: " + employeeId);
        if (employeeId == null) {
          setTimeout(() => {
            this.setState({
              isLoading: false,
            }); 
          }, 1000);
          setTimeout(() => {
            alert("No account found, please register");
          }, 1200);
        } else {
          try {
            password = await AsyncStorage.getItem("password");
            console.log("got password: " + password);
          } catch (e) {
            console.log("Exception get password: " + e);
          }
          setTimeout(() => {
            this.setState({
              isLoading: false,
            });
            if (
              this.state.employeeID == employeeId &&
              this.state.password == password
            ) {
              this.storeState("true");
              this.props.navigation.navigate("HomeScreen");
            } else {
              alert("Authentication failed, Invalid credentials");
            }
          }, 1500);
        }
      } catch (e) {
        console.log("Exception get employeeID: " + e);
      }
    }
  };

  storeState = async (value) => {
    try {
      await AsyncStorage.setItem("isLoggedIn", value);
    } catch (e) {
      // error reading value
    }
  };

  render() {
    const textInputChange = (value) => {
      if (value != 0) {
        this.setState({
          employeeID: value,
          checkTextInputChange: true,
        });
      } else {
        this.setState({
          employeeID: value,
          checkTextInputChange: false,
        });
      }
    };

    const handlePasswordChange = (value) => {
      this.setState({
        password: value,
        toggle: false,
      });
    };

    const updateSecureTextEntry = () => {
      this.setState({
        secureTextEntry: !this.state.secureTextEntry,
      });
    };

    return (
      <View style={styles.container}>
        <Loader loading={this.state.isLoading} />
        <StatusBar backgroundColor="dodgerblue" barStyle="light-content" />
        <View style={styles.header}>
          <Text style={styles.text_header}>Welcome!</Text>
          <Text style={styles.text_header_s}>Enter your details below</Text>
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.text_footer}>Employee ID</Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#05375A" size={20} />
            <TextInput
              placeholder="Enter employee ID"
              autoCapitalize="none"
              style={styles.textInput}
              onChangeText={(value) => textInputChange(value)}
            />
            {this.state.checkTextInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>
          <Text style={[styles.text_footer, { marginTop: 35 }]}>Password</Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375A" size={20} />
            <TextInput
              placeholder="Enter password"
              autoCapitalize="none"
              secureTextEntry={this.state.secureTextEntry ? true : false}
              style={styles.textInput}
              onChangeText={(value) => handlePasswordChange(value)}
            />
            <TouchableOpacity onPress={() => this.updateSecureTextEntry()}>
              {this.state.secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.signIn}
              onPress={() =>
                // this.props.navigation.navigate("HomeScreen")
                this.handleLogin()
              }
            >
              <LinearGradient
                colors={["#304F9F", "#03A3F4"]}
                style={styles.signIn}
              >
                <Text style={[styles.textSign, { color: "#fff" }]}>
                  Sign In
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("SignUpScreen")}
              style={[
                styles.signIn,
                { borderColor: "#303F9F", borderWidth: 1, marginTop: 15 },
              ]}
            >
              <Text style={[styles.textSign, { color: "#303F9F" }]}>
                Register
              </Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "dodgerblue",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_header_s: {
    marginTop: 10,
    color: "#fff",
    fontWeight: "normal",
    fontSize: 12,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
