import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Platform,
  StatusBar,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import GlobalEmployee from "../models/Employee"; //get new instance
import Loader from "../components/Loader";
import AsyncStorage from "@react-native-community/async-storage";

class SignUpScreen extends Component {
  state = {
    employeeID: "",
    password: "",
    confirmPassword: "",
    checkTextInputChange: false,
    secureTextEntry: true,
    confirmSecureTextEntry: true,
    isLoading: false,
  };

  constructor(props) {
    super(props);
  }

  textInputChange = (value) => {
    if (value != 0) {
      this.setState({
        employeeID: value,
        checkTextInputChange: true,
      });
    } else {
      this.setState({
        employeeID: value,
        checkTextInputChange: false,
      });
    }
  };

  handlePasswordChange = (value) => {
    this.setState({
      password: value,
      toggle: false,
    });
  };

  handleConfirmPasswordChange = (value) => {
    this.setState({
      confirmPassword: value,
      toggle: false,
    });
  };

  updateSecureTextEntry = () => {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  };

  updateConfirmSecureTextEntry = () => {
    this.setState({
      confirmSecureTextEntry: !this.state.confirmSecureTextEntry,
    });
  };

  saveData = () => {
    if (
      this.state.password == "" ||
      this.state.confirmPassword == "" ||
      this.state.employeeID == ""
    ) {
      alert("Please fill all fields");
    } else {
      if (this.state.password != this.state.confirmPassword) {
        alert("Sorry, Passwords must match");
      } else {
        this.setState({
          isLoading: true,
        });
        GlobalEmployee.employeeID = this.state.employeeID;
        GlobalEmployee.password = this.state.password;
        console.log("EMPLOYEE_ID: " + GlobalEmployee.employeeID);
        console.log("PASSWORD: " + GlobalEmployee.password);
        this.storeData(GlobalEmployee.employeeID, GlobalEmployee.password);
        this.storeState("true");
        setTimeout(() => {
          this.setState({
            isLoading: false,
          });
          this.props.navigation.navigate("HomeScreen");
        }, 2000);
      }
    }
  };

  storeData = async (employeeID, password) => {
    try {
      await AsyncStorage.setItem("employeeID", employeeID);
    } catch (e) {
      alert(e);
    }
    try {
      await AsyncStorage.setItem("password", password);
    } catch (e) {
      alert(e);
    }
  };

  storeState = async (value) => {
    try {
      await AsyncStorage.setItem("isLoggedIn", value);
    } catch (e) {
      // error reading value
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Loader loading={this.state.isLoading} />
        <StatusBar backgroundColor="dodgerblue" barStyle="light-content" />
        <View style={styles.header}>
          <Text style={styles.text_header}>Register Here!</Text>
          <Text style={styles.text_header_s}>Enter your details below</Text>
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.text_footer}>Employee ID</Text>
          <View style={styles.action}>
            <FontAwesome name="user-o" color="#05375A" size={20} />
            <TextInput
              placeholder="Enter employee ID"
              autoCapitalize="none"
              style={styles.textInput}
              value={this.state.employeeID}
              onChangeText={(employeeID) => this.textInputChange(employeeID)}
            />
            {this.state.checkTextInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>
          <Text style={[styles.text_footer, { marginTop: 35 }]}>Password</Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375A" size={20} />
            <TextInput
              placeholder="Enter password"
              autoCapitalize="none"
              secureTextEntry={this.state.secureTextEntry ? true : false}
              style={styles.textInput}
              value={this.state.password}
              onChangeText={(value) => this.handlePasswordChange(value)}
            />
            <TouchableOpacity onPress={this.updateSecureTextEntry}>
              {this.state.secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>
          <Text style={[styles.text_footer, { marginTop: 35 }]}>
            Confirm Password
          </Text>
          <View style={styles.action}>
            <Feather name="lock" color="#05375A" size={20} />
            <TextInput
              placeholder="Confirm your password"
              autoCapitalize="none"
              secureTextEntry={this.state.confirmSecureTextEntry ? true : false}
              style={styles.textInput}
              value={this.state.confirmPassword}
              onChangeText={(value) => this.handleConfirmPasswordChange(value)}
            />
            <TouchableOpacity onPress={this.updateConfirmSecureTextEntry}>
              {this.state.confirmSecureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="grey" size={20} />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.signIn}
              onPress={
                () => this.saveData()
                //  navigation.navigate("HomeScreen")
              }
            >
              <LinearGradient
                colors={["#304F9F", "#03A3F4"]}
                style={styles.signIn}
              >
                <Text style={[styles.textSign, { color: "#fff" }]}>
                  Register
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("SignInScreen")}
              style={[
                styles.signIn,
                { borderColor: "#303F9F", borderWidth: 1, marginTop: 15 },
              ]}
            >
              <Text style={[styles.textSign, { color: "#303F9F" }]}>
                Sign In
              </Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    );
  }
}

export default SignUpScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "dodgerblue",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_header_s: {
    marginTop: 10,
    color: "#fff",
    fontWeight: "normal",
    fontSize: 12,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
