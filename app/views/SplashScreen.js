import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  TouchableOpacity,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import * as SQLite from "expo-sqlite";

const db = SQLite.openDatabase("datab.db");
console.log("----------- 1- Create/Open DataBase---------------");

class SplashScreen extends Component {
  state = {
    employee_id: "sample_id",
    company: "sample_company",
    rate: 430,
    date: "sample_date",
    created_date: "sample_created_date",
    updated_date: "sample_updated_date",
  };
  componentDidMount() {
    db.transaction((tx) => {
      console.log("----------- 1- Create table---------------");
      tx.executeSql(
        "create table if not exists timelogs (id INTEGER PRIMARY KEY AUTO_INCREMENT,employee_id text, rate int, company text, date text, start_time text, end_time text, created_date text, updated_date text);"
      );
    });

    if (this.getState() == "true") {
      this.props.navigation.navigate("HomeScreen");
    } else if (this.getState() == "false") {
      this.props.navigation.navigate("SignInScreen");
    } else {
      // do nothing... just stay here
    }
  }
  constructor(props) {
    super(props);
  }

  getState = async () => {
    try {
      const value = await AsyncStorage.getItem("isLoggedIn");
      if (value !== null) {
        return value;
      } else {
        return null;
      }
    } catch (e) {
      // error reading value
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="dodgerblue" barStyle="light-content" />

        <View style={styles.header}>
          <Animatable.Image
            animation="bounceIn"
            source={require("../assets/images/logo.png")}
            style={styles.logo}
          />
          <Animatable.Text animation="fadeInDown" style={styles.textLogo}>
            {" "}
            LogEx{" "}
          </Animatable.Text>
        </View>
        <Animatable.View style={styles.footer} animation="fadeInUpBig">
          <Text style={styles.title}>Log your time anywhere anytime!</Text>
          <Text style={styles.text}> Sign in with your account</Text>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("SignInScreen")}
            >
              <LinearGradient
                colors={["#303F9F", "#03A9F4"]}
                style={styles.gradient}
              >
                <Text style={styles.textSign}>Sign In</Text>
                <MaterialIcons name="navigate-next" color="#fff" size={24} />
              </LinearGradient>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("SignUpScreen")}
          >
            <Animatable.Text style={styles.text2} animation="fadeInLeftBig">
              {" "}
              or Register here
            </Animatable.Text>
          </TouchableOpacity>
        </Animatable.View>
      </View>
    );
  }
}

export default SplashScreen;

const { height } = Dimensions.get("screen");
const height_logo = height * 0.18;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "dodgerblue",
  },
  header: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  footer: {
    flex: 1,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: "#05375a",
    fontSize: 30,
    fontWeight: "bold",
  },
  text: {
    textAlign: "left",
    color: "grey",
    marginTop: 5,
  },
  text2: {
    textAlign: "left",
    color: "grey",
    marginTop: 40,
    fontSize: 22,
    color: "#757575",
    fontWeight: "bold",
  },
  textLogo: {
    color: "white",
    marginTop: 10,
    fontSize: 30,
    fontWeight: "bold",
  },
  button: {
    alignItems: "flex-end",
    marginTop: 10,
  },
  button2: {
    alignItems: "flex-start",
    marginTop: 10,
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    flexDirection: "row",
  },
  gradient: {
    width: 150,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    flexDirection: "row",
  },
  textSign: {
    color: "white",
    fontWeight: "bold",
  },
});
